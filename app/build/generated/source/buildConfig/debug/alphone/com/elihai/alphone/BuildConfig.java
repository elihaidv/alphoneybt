/**
 * Automatically generated file. DO NOT MODIFY
 */
package alphone.com.elihai.alphone;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.elihai.alphone";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "3";
}
