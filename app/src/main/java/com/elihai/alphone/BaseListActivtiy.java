package com.elihai.alphone;

import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;

import alphone.com.elihai.alphone.R;

public abstract class BaseListActivtiy extends AppCompatActivity {
    protected ArrayAdapter<Person> persons;
    private Person selected;

    protected FirebaseDatabase database = FirebaseDatabase.getInstance();
    protected DatabaseReference myRef = database.getReference("alphone");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        persons = new NamesAdapter(this, R.layout.person_item);


        //Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                persons.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    persons.add(child.getValue(Person.class));
                }
                persons.sort(new PersonComp());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    protected class ItemClick implements new AdapterView.OnItemClickListener {

        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            selected = (Person) arg0.getItemAtPosition(position);
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + selected.getPhone()));
            Toast.makeText(getApplicationContext(), selected.getName(), Toast.LENGTH_LONG).show();
            startActivity(intent);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
        menu.setHeaderTitle(selected.getName());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add: {
                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.NAME, selected.getName());
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, selected.getPhone());
                startActivity(intent);
                return true;
            }
            case R.id.copy: {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(selected.getPhone());
                Toast.makeText(BaseListActivtiy.this, "המספר הועתק", Toast.LENGTH_SHORT).show();
                return true;
            }
            case R.id.sms:{
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:+972"+selected.getPhone()));
                startActivity(sendIntent);
                return true;
            }
            case R.id.share:{
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, selected.toString());
                startActivity(Intent.createChooser(sharingIntent, "שתף מספר"));
            }
        }

        return super.onContextItemSelected(item);
    }

    protected class PersonComp implements Comparator<Person> {
        @Override
        public int compare(Person person, Person person2) {
            return person.getName().compareTo(person2.getName());
        }
    }
}
