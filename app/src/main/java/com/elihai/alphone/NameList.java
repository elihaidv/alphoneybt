package com.elihai.alphone;

import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import alphone.com.elihai.alphone.R;

public class NameList extends BaseListActivtiy {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_list);
        ListView nl = (ListView) findViewById(R.id.NameList);


        nl.setAdapter(persons);
        nl.setClickable(true);
        registerForContextMenu(nl);

        nl.setOnItemClickListener(new ItemClick());

    }
}
