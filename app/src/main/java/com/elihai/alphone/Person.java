package com.elihai.alphone;

/**
 * Created by elihai on 9/10/17.
 */

public class Person {
    private String name;
    private String phone;

    public Person(){
        this.phone = "";
        this.name = "";
    }

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name + ": " + this.phone;
    }
}
