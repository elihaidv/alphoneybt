package com.elihai.alphone;

/**
 * Created by elihai on 9/11/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import alphone.com.elihai.alphone.R;

public class NamesAdapter extends ArrayAdapter<Person> {

    private int layoutResource;

    public NamesAdapter(Context context, int layoutResource) {
        super(context, layoutResource);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        final Person person = getItem(position);

        if (person != null) {
            TextView name = (TextView) view.findViewById(R.id.nameItem);

            name.setText(person.getName());

        }

        return view;
    }
}
