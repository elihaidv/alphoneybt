package com.elihai.alphone;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import java.util.Comparator;

import alphone.com.elihai.alphone.R;

public class MainActivity extends BaseListActivtiy {

    private NamesAdapter personsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = (ListView) findViewById(R.id.resultList);
        personsAdapter = new NamesAdapter(this, R.layout.person_item);
        listView.setAdapter(personsAdapter);

        final EditText editText=(EditText)findViewById(R.id.edit_text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                personsAdapter.clear();
                if (!s.toString().equals("")) {
                    for (int i = 0; i < persons.getCount(); i++) {
                        if (persons.getItem(i).getName().contains(s)) {
                            personsAdapter.add(persons.getItem(i));
                        }
                    }
                }
             }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listView.setOnItemClickListener(new ItemClick());
        registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent instructions=new Intent(this, Ins.class);
            startActivity(instructions);
            return true;
        }
        if(id==R.id.list){
            Intent list =new Intent(this, NameList.class);
            startActivity(list);
            return true;
        }
        if(id==R.id.shabbes){
            Intent shabbes =new Intent(this, Shabbes.class);
            startActivity(shabbes);
            return true;
        }
        if(id==R.id.tfilot){
            Intent tfilot =new Intent(this, Tfilot.class);
            startActivity(tfilot);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addContact(View v){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.new_contact_dialog);
        dialog.setTitle("הוסף איש קשר חדש");

        dialog.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRef.push().
                        setValue(new Person(
                                ((EditText)dialog.findViewById(R.id.new_name)).getText().toString(),
                                ((EditText)dialog.findViewById(R.id.new_phone)).getText().toString()));
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
