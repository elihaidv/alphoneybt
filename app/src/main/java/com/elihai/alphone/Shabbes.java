package com.elihai.alphone;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import alphone.com.elihai.alphone.R;

public class Shabbes extends AppCompatActivity {
    private SharedPreferences sp;
    private Bitmap bitmap;
    private CountDownLatch latch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shabbes);
        final TouchImageView image = (TouchImageView) this.findViewById(R.id.shabbatYeshiva);

        File pic = new File(getFilesDir() + "/pic.jpg");
        if (pic.exists()) {
            image.setImageURI(Uri.fromFile(pic));
        }


        latch = new CountDownLatch(1);

//        Thread thread = new Runnable(){
//            @Override
//            public void run() {
//                try {
//                    URL url = new URL("http://pastebin.com/raw/U4D1zfcA");
//                    Scanner in = new Scanner(url.openStream());
//                    sp=getPreferences(MODE_PRIVATE);
//                    sp.edit().putString("shabatot",in.nextLine()).commit();
//
//                    String s = sp.getString("shabatot","");
//                    InputStream is = new java.net.URL(s).openStream();
//                    bitmap = BitmapFactory.decodeStream(is);
//                    latch.countDown();
//
//                    FileOutputStream out = new FileOutputStream(getFilesDir() + "/pic.jpg");
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    latch.countDown();
//                }
//            }
//        })
//        thread.start();
        try {
            latch.await();
            if (bitmap != null) {
                image.setImageBitmap(bitmap);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
