package com.elihai.alphone;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import alphone.com.elihai.alphone.R;

public class Tfilot extends AppCompatActivity {

    private static final String YOM_HOL = "yomhol";
    private static final String SHABBAT = "shabbat";

    private final String SITE = "http://www.tifart.com/B/";
    private WebView webView;
    private LinearLayout linearLayout;
    private SharedPreferences sp;
    private CountDownLatch latch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tfilot);
        webView = (WebView) findViewById(R.id.webview);
        webView.setVisibility(View.GONE);
        sp = getPreferences(MODE_PRIVATE);

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError (WebView view, int errorCode, String description, String failingUrl) {
                webView.loadData("<h1> Please Wait For Page Loading... <h1>", "text/html", "UTF-8");
            }
        });

        latch = new CountDownLatch(1);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int place = 0;
                    Document doc = Jsoup.connect(SITE + "tefilot.html").get();

                    Elements newsHeadlines = doc.select("p");
                    for (int i = 0; i < newsHeadlines.size(); i++) {
                        if (newsHeadlines.get(i).toString().contains("אברהם אוהבי")) {
                            place = i;
                        }

                    }

                    Element elm = newsHeadlines.get(place + 1).getElementsByTag("img").get(0);
                    sp.edit().putString(SHABBAT, SITE + elm.attr("src")).commit();

                    elm = newsHeadlines.get(place + 2).getElementsByTag("img").get(0);
                    sp.edit().putString(YOM_HOL, SITE + elm.attr("src")).commit();
                } catch (IOException e) {
                }
                latch.countDown();
            }
        });
        thread.start();

        linearLayout = (LinearLayout) findViewById(R.id.buttonsL);
    }

    public void shabbes(View view) {
        webView.loadUrl(load(SHABBAT));
    }

    public void yomhol(View view) {
        webView.loadUrl(load(YOM_HOL));
    }

    private String load(String resource) {

        String url = sp.getString(resource, "");

        if (url.equals("")) {
            try {

                latch.await();
                url = sp.getString(resource, "");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        webView.getSettings().setBuiltInZoomControls(true);
        webView.setInitialScale(130);
        linearLayout.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        return url;
    }

}
